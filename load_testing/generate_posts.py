import random
import time
from multiprocessing.pool import ThreadPool

import pandas as pd
import requests


def add_followers(user_id):
    for urn in random.choices(urns, k=30):
        response = requests.post('http://192.168.99.101:5000/api/new_post',
                                 json={'user_id': int(user_id), 'urn': urn[1:-2]})
        if response.status_code != 200:
            print(response.status_code)


df = pd.read_csv('ids', delimiter=' ')
urns = open('urns').readlines()
print(time.time())
with ThreadPool() as p:
    p.map(add_followers, df["Id"])
print(time.time())