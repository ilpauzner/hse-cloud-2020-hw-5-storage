import random
import time
from multiprocessing.pool import ThreadPool

import pandas as pd
import requests


def add_followers(user_id):
    for follower in random.choices(df["Id"], k=10):
        response = requests.post('http://192.168.99.101:5000/api/follow',
                                 json={'follower_id': int(follower), 'following_id': int(user_id)})
        if response.status_code != 200:
            print(response.status_code)


df = pd.read_csv('ids', delimiter=' ')
print(time.time())
with ThreadPool() as p:
    p.map(add_followers, df["Id"])
print(time.time())