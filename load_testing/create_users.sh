#! /bin/sh

for _ in $(seq 1 10000)
do
  name=$(head -c 10 /dev/urandom | sha1sum | cut -d ' ' -f 1 | head -c 10)
  email="$name@example.com"
  password="$name"
  id=$(curl http://192.168.99.101:5000/api/create_user -X POST -H "Content-Type: application/json" --data "{\"name\":\"$name\",\"email\":\"$email\",\"password\":\"$password\"}" | jq .id)
  echo "$id $email $password" >> ids
done