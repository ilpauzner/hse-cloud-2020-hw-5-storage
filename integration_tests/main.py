import os
import time

import boto3
import requests

time.sleep(5)

endpoint = os.getenv('SERVICE_ENDPOINT')
name = 'john_doe'
email = 'john_doe@example.com'
password = 'john_doe_password'

response = requests.post(endpoint + '/api/create_user', json={'name': name, 'email': email, 'password': password})
assert response.status_code == 200

user_id = int(response.json()['id'])
assert user_id == 1
print('create_user OK')

response = requests.post(endpoint + '/api/uploader', files={'file': open('bunny.jpg', 'rb')})
assert response.status_code == 200
print('uploader file OK')

urn = response.json()['urn']
print(urn)

image = requests.get(urn)
assert image.status_code == 200

downloaded_image = image.content
read_image = open('bunny.jpg', 'rb').read()
assert downloaded_image == read_image
print('Image from link and image on disk are equal')

session = boto3.session.Session()
s3 = session.client(service_name='s3', endpoint_url=os.getenv('AWS_ENDPOINT'))
bucket_name = os.getenv('BUCKET_NAME')

objects = s3.list_objects(Bucket=bucket_name)['Contents']
minio_image = s3.get_object(Bucket=bucket_name, Key=objects[0]['Key'])['Body'].read()
assert minio_image == read_image
print('Image from minio and image on disk are equal')
