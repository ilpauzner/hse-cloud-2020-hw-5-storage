import os

import redis

auth_feed_cache = redis.Redis(host=os.getenv('CACHE_HOST'), decode_responses=True, encoding='utf-8', db=1)
rest_feed_cache = redis.Redis(host=os.getenv('CACHE_HOST'), decode_responses=True, encoding='utf-8', db=2)
main_trending_cache = redis.Redis(host=os.getenv('CACHE_HOST'), decode_responses=True, encoding='utf-8', db=3)
rest_trending_cache = redis.Redis(host=os.getenv('CACHE_HOST'), decode_responses=True, encoding='utf-8', db=4)
