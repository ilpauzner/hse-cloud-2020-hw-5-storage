import os

import boto3

session = boto3.session.Session()
s3 = session.client(service_name='s3', endpoint_url=os.getenv('AWS_ENDPOINT'))
bucket_name = os.getenv('BUCKET_NAME')

try:
    # Создать новый бакет
    s3.create_bucket(Bucket=bucket_name)
except Exception as e:
    if 'BucketAlreadyOwnedByYou' in str(type(e)):
        pass
    else:
        raise e
